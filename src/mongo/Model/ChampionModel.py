class ChampionModel:
    def __init__(self, Id, name, alias, title, short_bio,
                 tactical_info, play_style_info, roles,
                 skins, passive, spells):

        self.Id = Id
        self.name = name
        self.alias = alias
        self.title = title
        self.short_bio = short_bio
        self.tactical_info = tactical_info
        self.play_style_info = play_style_info
        self.roles = roles
        self.skins = skins
        self.passive = passive
        self.spells = spells
