import logging
import os
from pymongo import MongoClient

CONNECTION_STRING = os.getenv('CONNECTION_URL')
DB_NAME = os.getenv('DATABASE_NAME')


class MongoConfiguration:
    def mongo(self):
        try:
            client = MongoClient(CONNECTION_STRING)
            db = client.test
            logging.info("Connected successfully!")
        except:
            logging.error("Error conencting to MongoDB")

        data_base = client[DB_NAME]
        champ_collection = data_base['champion_list']

        
