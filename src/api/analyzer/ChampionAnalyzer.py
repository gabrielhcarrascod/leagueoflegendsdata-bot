import json

from ..requests.ChampionRotationRequests import *


class ChampionAnalyzer:

    def replace_champ_id_for_name(self):
        free_champ_ids = []

        champ_list_id_only = ChampionRotationRequests().get_champion_rotation()
        champ_list_data = ChampionRotationRequests()

        free_champion_ids = champ_list_id_only["freeChampionIds"]
        free_champion_ids_for_new_players = champ_list_id_only["freeChampionIdsForNewPlayers"]
        max_new_player_level = champ_list_id_only["maxNewPlayerLevel"]

        for champ_id in free_champion_ids:
            fetch_champ = champ_list_data.get_champion_name_by_id(str(champ_id))
            free_champ_ids.append(fetch_champ["name"])

        free_champs = dict({"freeChampions": free_champ_ids})

        return json.dumps(free_champs, indent=4)
