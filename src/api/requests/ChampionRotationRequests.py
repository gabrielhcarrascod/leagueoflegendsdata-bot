import requests
import os
from dotenv import load_dotenv

load_dotenv()

TOKEN_KEY = os.getenv("TOKEN_KEY")

DDRAGON_URI = os.getenv("DDRAGON")
BASE_PATH = os.getenv("BASE_PATH")
URI = os.getenv("URI")


class ChampionRotationRequests:

    def get_champion_rotation(self):
        api_url = BASE_PATH + URI
        headers = {
            "Content-Type": "application/jso",
            "X-Riot-Token": "{}".format(TOKEN_KEY),
        }
        champs_rotation = requests.get(api_url,
                                       headers=headers)

        return champs_rotation.json()


    def get_champion_name_by_id(self, id):
        api_url = DDRAGON_URI + id + ".json"
        headers = {
            "Content-Type": "application/jso"
        }
        champ_list = requests.get(api_url,
                                  headers=headers)

        return champ_list.json()

