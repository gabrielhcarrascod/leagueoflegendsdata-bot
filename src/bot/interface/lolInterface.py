import asyncio
import discord
import os
from dotenv import load_dotenv
from src.api.analyzer.ChampionAnalyzer import *

TOKEN = os.getenv("TOKEN_BOT")

intents = discord.Intents.default()
intents.message_content = True

client = discord.Client(intents=intents)

champion_analyzer = ChampionAnalyzer()


class ChampionRotation:

    @client.event
    async def on_ready(self):
        print(f'We have logged in ad {client.user}'
              .format(client))
        await asyncio.sleep(1)

    @client.event
    async def on_message(message):
        if message.author == client.user:
            return
        if message.content.startswith("$free-champs"):
            champ_list = champion_analyzer.replace_champ_id_for_name()
            await message.channel.send(champ_list)

    client.run(TOKEN)
