# LeagueOfLegendsData-Bot

LolDaBot is an small Python application that allows a lol player, using Discord to ask for in-game and game history
data through the Discord server channel.

## Getting started

    1.- Request developer permissions to an admin
    2.- Install python3
        2.1 - install discord package
        2.2 - import load_dotenv

## Running & Testing

NOTES: - The oldest python version this project has been tested with 3.10.6 - Code ran in Ubuntu 22.4 and Windows 10

To be able to run and test it is necesary to get Token from the bot-owner and be included in the Discord Server.

## Authors:

    - Gabriel Hernan Carrasco Diaz - gabrielhcarrascod@gmail.com
    - Pichu - email
